<?php

/**
 * @file
 * Provides the Google Url Shortener administration settings.
 */

/**
 * Form callback; administrative settings for Google Url Shortener.
 */
function urlshort_admin_settings() {
  // Load the urlshort class. Error if class does not load.
  if (!_urlshort_load_library()) {
    drupal_set_message(t('Error loading urlshortclass.'), 'error');
    return FALSE;
  }

  global $base_url;

  $query = db_select('node', 'n');
  $query->leftJoin('urlshort', 'u', 'n.nid = u.nid');
  $query->isNull('u.nid');
  $query->addField('n', 'nid', 'nnid');
  $results = $query->execute()->fetchAll();
  $count = count($results);

  $form = array();
  $form['urlshort_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('urlshort_api_key', ''),
    '#maxlength' => 40,
    '#description' => t('The public key given to you when you create a project in Google Console API.'),
    '#required' => TRUE,
  );
  $form['urlshort_base_url'] = array(
	'#type' => 'hidden',
	'#value' => variable_get('urlshort_base_url', $base_url),
  );
  $form['urlshort_bulk_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configure bulk generate short url existing nodes'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['urlshort_bulk_container']['urlshort_activate_bulk'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate short url bulk generate'),
    '#default_value'=> variable_get('urlshort_activate_bulk', ''),
    '#description' => t('When you select this option , you will activate the massive generation of short urls for existing nodes every time you run the cron.'),
  );
  $form['urlshort_bulk_container']['urlshort_limit_bulk'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit to bulk generate short url'),
    '#default_value' => variable_get('urlshort_limit_bulk', ''),
    '#maxlength' => 40,
    '#description' => t('Limit to generate short url existing nodes.'),
    '#required' => TRUE,
  );
  $form['urlshort_bulk_container']['urlshort_rest_nodes'] = array(
    '#type' => 'item',
    '#title' => t('Rest bulk generate: '),
    '#markup' => $count.' nodes',
  );

  return system_settings_form($form);
}